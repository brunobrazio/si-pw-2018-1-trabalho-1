import { Injectable, ApplicationRef } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class ApiService {

    router: Router;
    nickname: string;
    logged: boolean = false;
    channels: Object[] = [];
    section: string = "server";
    activeChannel: Object;

    constructor(router: Router) {
        this.router = router;
        this.randomChannels();
    }
    
    randomChannels() {
        this.channels.push({'name': 'Amizade', 'nicks': this.randomNicks(), 'messages': [] });
        this.channels.push({'name': 'Futebol', 'nicks': this.randomNicks(), 'messages': [] });
        this.channels.push({'name': 'Música', 'nicks': this.randomNicks(), 'messages': [] });
        this.channels.push({'name': 'Namoro', 'nicks': this.randomNicks(), 'messages': [] });
        this.channels.push({'name': 'Política', 'nicks': this.randomNicks(), 'messages': [] });
        this.channels.push({'name': 'Religião', 'nicks': this.randomNicks(), 'messages': [] });
        this.channels.push({'name': 'TV', 'nicks': this.randomNicks(), 'messages': [] });
    }

    randomNicks() {
        let nicks = ['Pedro', 'Tiago', 'Mônica', 'Raquel', 'Sabrina', 'Júnior', 'Teresa', 'Henrique', 'Leonardo', 'Jéssica', 'Larissa', 'Júlia', 'Roberto', 'Mariana'];
        let max = this.randomInteger(1, nicks.length);
        let generatedNicks = [];

        for (let i = 0; i < max; i++) {
            let index = this.randomInteger(0, nicks.length-1);
            generatedNicks.push(nicks.splice(index, 1));
        }

        return generatedNicks;
    }

    randomInteger(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    login(nickname: string) {
        this.nickname = nickname;
        var self = this;

        setTimeout(function(){
            self.logged = true;
            self.router.navigateByUrl('/chat');    
        }, 3000);
    }

    logout() {
        this.nickname = "";
        this.logged = false;
        this.router.navigateByUrl('/login');    
    }

    isLogged() {
        return this.logged;
    }

    setSection(section: string) {
        if (this.section != "server") {
            this.activeChannel = this.getChannelByName(this.section);
            this.activeChannel['messages'] = [];
        }

        if (section != "server") {
            this.activeChannel = this.getChannelByName(section);
            this.addMessageOnChannel(null, this.nickname + " entrou na sala.");
        }

        this.section = section;
    }

    getChannelByName(name: string) {
        return this.channels.find(c => c['name'] === name);
    }

    sendMessage(message: string) {
        this.addMessageOnChannel(this.nickname, message);
    }

    addMessageOnChannel(sender: string, message: string) {
        let date = new Date().toLocaleTimeString();
        let senderText = (sender == null) ? "" : "<b>" + sender + ":</b> ";
        let messageText = (sender == null) ? "<b>"+ message + "</b>" : message;
        this.activeChannel['messages'].push("<i>" + date + "</i>" + "&emsp;" + senderText + messageText);
    }
}
