import 'materialize-css';
import { MaterializeDirective, MaterializeAction } from "angular2-materialize";
import { Component, EventEmitter } from "@angular/core"

@Component({
    selector: 'dialog',
    template: `
        <div id="modal" class="modal bottom-sheet" materialize="modal" [materializeParams]="modelParams" [materializeActions]="modalActions">
            <div class="modal-content">
                <h4>Modal Header 1</h4>
                <p>A bunch of text</p>
            </div>
            <div class="modal-footer">
                <a class="waves-effect waves-green btn-flat" (click)="closeModal()">Close</a>
                <a class="modal-action modal-close waves-effect waves-green btn-flat">Agree</a>
            </div>
        </div>
    `
})
export class DialogComponent {

    modalActions = new EventEmitter<string | MaterializeAction>();
    params = []

    modelParams = [
        {
            dismissible: false,
            complete: function () { console.log('Closed'); }
        }
    ]

    openModal() {
        this.modalActions.emit({ action: "modal", params: ['open'] });
    }

    closeModal() {
        this.modalActions.emit({ action: "modal", params: ['close'] });
    }

}
