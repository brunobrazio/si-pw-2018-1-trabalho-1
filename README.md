# Trabalho 1 - WebChat em Angular (front-end apenas)

Projeto de Webchat utilizando HTML, CSS, Materialize, Angular e Typescript.  
Programação para Web, 2018/1 - Sistemas da Informação, UFG.  
Grupo: Bruno Braz Silveira e Diego Zanata Gritti.

## Como Utilizar

O projeto foi desenvolvido em Angular, siga os comandos básicos do Angular:  

* Instalação do angular-cli.
* Baixe as dependências do projeto (npm install).
* Rode o projeto (ng serve ou ng build).

## Exemplo

<a href="https://brunobraz.io/ufg/ppw/trabalho1/index.html">https://brunobraz.io/ufg/ppw/trabalho1</a>

## Autores

* **Bruno Braz Silveira** - <a href="https://brunobraz.io">brunobraz.io</a>
* **Diego Zanata Gritti**
